-- A.
SELECT * FROM artists WHERE name LIKE "%d%";

-- B.
SELECT * FROM songs WHERE length < 230;

-- C.
SELECT name, song_name, length FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs on albums.id = songs.album_id;

-- D.
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE name LIKE "%a%";

-- E.
SELECT * FROM songs ORDER BY song_name DESC LIMIT 4;

-- F.
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs on albums.id = songs.album_id
    ORDER BY album_title DESC, song_name ASC;
    

   